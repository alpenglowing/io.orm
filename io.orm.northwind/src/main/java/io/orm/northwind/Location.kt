package io.orm.northwind

import jakarta.persistence.*
import org.eclipse.persistence.annotations.UuidGenerator
import java.util.*

@Entity(name = "location")
@Table(name = "locations")
@UuidGenerator(name = "location_uuid")
class Location(
  @Id
  @GeneratedValue(generator = "location_uuid")
  @Convert(converter = Type.Uuid::class)
  var id: UUID? = null,

  var address: String? = null,
  var city: String? = null,
  var province: String? = null,
  var region: String? = null,
  var postalCode: String? = null,

  @OneToOne(targetEntity = Person::class)
  var person: Person?
)
