package io.orm.northwind

import jakarta.persistence.*
import jakarta.persistence.CascadeType.PERSIST
import jakarta.persistence.InheritanceType.JOINED
import org.eclipse.persistence.annotations.UuidGenerator
import java.time.LocalDate
import java.util.*

@Entity(name = "person")
@Table(name = "people")
@Inheritance(strategy = JOINED)
@UuidGenerator(name = "person_uuid")
abstract class Person(
  @Id
  @GeneratedValue(generator = "person_uuid")
  @Convert(converter = UUIDAttribute::class)
  var id: UUID? = null,

  val fullName: String?,

  @OneToOne(mappedBy = "locations", targetEntity = Location::class, cascade = [PERSIST])
  var location: Location? = null
)

@Entity
class Physical(
  var firstName: String? = null,
  var lastName: String? = null,
  var birthDate: LocalDate? = null,
  var fiscalCode: String? = null
) : Person(fullName = "$firstName $lastName")

@Entity
class Juridical(
  var name: String? = null,
  var vatNumber: String? = null
) : Person(fullName = name)
