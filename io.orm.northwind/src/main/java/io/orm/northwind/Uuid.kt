package io.orm.northwind

import jakarta.persistence.AttributeConverter
import jakarta.persistence.Converter
import java.util.*

sealed interface Type {
  @Converter
  object Uuid : AttributeConverter<UUID, String>, Type {
    override fun convertToDatabaseColumn(uuid: UUID) = uuid.toString()

    override fun convertToEntityAttribute(value: String): UUID = UUID.fromString(value)
  }
}
