package io.orm.model

import io.orm.model.type.UUIDAttribute
import jakarta.persistence.*
import org.eclipse.persistence.annotations.Cache
import org.eclipse.persistence.annotations.UuidGenerator
import java.util.*

@Entity
@Cache(refreshOnlyIfNewer = true)
@UuidGenerator(name = "address_uuid")
data class Address(
  @Id
  @GeneratedValue(generator = "address_uuid")
  @Convert(converter = UUIDAttribute::class)
  var id: UUID? = null,

  @Basic var city: String?,

  @Lob
  @Basic(fetch = FetchType.LAZY) var country: String?,

  @Basic var province: String?,

  @Basic var postalCode: String?,
  @Basic var street: String?
) {
}
