package io.orm.model

import io.orm.model.type.UUIDAttribute
import jakarta.persistence.*
import jakarta.persistence.CascadeType.PERSIST
import org.eclipse.persistence.annotations.UuidGenerator
import java.util.*

@Entity(name = "book")
@Table(name = "books")
@UuidGenerator(name = "book_uuid")
class Book(
  @Id
  @GeneratedValue(generator = "book_uuid")
  @Convert(converter = UUIDAttribute::class)
  val id: UUID? = null,

  var title: String? = null,

  @ManyToOne(cascade = [PERSIST])
  @JoinColumn(name = "publisher_id")
  var publisher: Publisher? = null,

  @ManyToMany(cascade = [PERSIST])
  @JoinTable(
    name = "writers",
    joinColumns = [JoinColumn(name = "book_id")],
    inverseJoinColumns = [JoinColumn(name = "author_id")]
  )
  var writers: MutableSet<Author> = mutableSetOf()
)

fun Book.writtenBy(vararg _authors: Author) = _authors.forEach { it.wrote(this) }.let { this }

fun Book.publishedBy(publisher: Publisher) = publisher.published(this).let { this }
