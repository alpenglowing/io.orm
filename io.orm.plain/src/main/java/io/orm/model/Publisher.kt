package io.orm.model

import io.orm.model.type.UUIDAttribute
import jakarta.persistence.*
import jakarta.persistence.CascadeType.PERSIST
import org.eclipse.persistence.annotations.UuidGenerator
import java.util.*

@Entity(name = "publisher")
@Table(name = "publishers")
@UuidGenerator(name = "publisher_uuid")
class Publisher(
  @Id
  @GeneratedValue(generator = "publisher_uuid")
  @Convert(converter = UUIDAttribute::class)
  var id: UUID? = null,

  var name: String? = null,

  @OneToMany(mappedBy = "publisher", targetEntity = Book::class, cascade = [PERSIST])
  var books: MutableSet<Book> = mutableSetOf()
)

fun Publisher.published(vararg _books: Book): Publisher {
  books.addAll(_books)
  _books.forEach { it.publisher = this }
  return this
}
