package io.orm.model

import jakarta.persistence.Embeddable
import jakarta.persistence.Temporal
import jakarta.persistence.TemporalType
import java.util.Calendar

@Embeddable
class EmploymentPeriod {
  @Temporal(TemporalType.DATE)
  var startDate: Calendar? = null

  @Temporal(TemporalType.DATE)
  var endDate: Calendar? = null
  fun setStartDate(year: Int, month: Int, date: Int) {
    if (startDate == null) {
      startDate = Calendar.getInstance()
    }
    startDate!![year, month] = date
  }

  fun setEndDate(year: Int, month: Int, date: Int) {
    if (endDate == null) {
      endDate = Calendar.getInstance()
    }
    endDate!![year, month] = date
  }
}
