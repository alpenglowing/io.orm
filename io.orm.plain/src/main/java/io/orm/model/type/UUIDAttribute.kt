package io.orm.model.type

import jakarta.persistence.AttributeConverter
import jakarta.persistence.Converter
import java.util.UUID
import java.util.Optional

@Converter
class UUIDAttribute : AttributeConverter<UUID, String> {
  override fun convertToDatabaseColumn(uuid: UUID) = uuid.toString()

  override fun convertToEntityAttribute(value: String): UUID = UUID.fromString(value)
}
