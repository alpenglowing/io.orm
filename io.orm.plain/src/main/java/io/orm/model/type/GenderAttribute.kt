package io.orm.model.type

import io.orm.model.Gender
import jakarta.persistence.AttributeConverter
import jakarta.persistence.Converter

@Converter(autoApply = true)
class GenderAttribute : AttributeConverter<Gender, String> {
  override fun convertToDatabaseColumn(gender: Gender) =
    when (gender) {
      Gender.Male -> "M"
      Gender.Female -> "F"
    }

  override fun convertToEntityAttribute(gender: String): Gender =
    when (gender) {
      "M" -> Gender.Male
      "F" -> Gender.Female
      else -> throw IllegalArgumentException("Invalid gender code: $gender")
    }
}
