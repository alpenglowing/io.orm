package io.orm.model

import io.orm.model.type.GenderAttribute
import io.orm.model.type.UUIDAttribute
import jakarta.persistence.*
import org.eclipse.persistence.annotations.PrivateOwned
import org.eclipse.persistence.annotations.UuidGenerator
import org.eclipse.persistence.config.HintValues
import org.eclipse.persistence.config.QueryHints
import java.util.*

val DefaultUUID: UUID = UUID.randomUUID()

@Entity
@SecondaryTable(name = "SALARY")
@UuidGenerator(name = "employee_uuid")
@NamedQueries(
  NamedQuery(name = "Employee.findAll", query = "SELECT e FROM Employee e ORDER BY e.id"),
  NamedQuery(name = "Employee.findByName", query = "SELECT e FROM Employee e WHERE e.firstName LIKE :firstName AND e.lastName LIKE :lastName"),
  NamedQuery(name = "Employee.count", query = "SELECT COUNT(e) FROM Employee e"),
  NamedQuery(name = "Employee.countByName", query = "SELECT COUNT(e) FROM Employee e WHERE e.firstName LIKE :firstName AND e.lastName LIKE :lastName"),
  NamedQuery(name = "Employee.idsIn", query = "SELECT e FROM Employee e WHERE e.id IN :IDS ORDER BY e.id", hints = [QueryHint(name = QueryHints.QUERY_RESULTS_CACHE, value = HintValues.TRUE)])
)
data class Employee(
  @Id
  @Column(name = "EMP_ID")
  @Convert(converter = UUIDAttribute::class)
  @GeneratedValue(generator = "employee_uuid")
  var id: UUID? = null,

  @Column(name = "F_NAME")
  var firstName: String? = null,

  @Column(name = "L_NAME")
  var lastName: String? = null
) {


  /**
   * Gender mapped using Basic with an ObjectTypeConverter to map between
   * single char code value in database to enum. JPA only supports mapping to
   * the full name of the enum or its ordinal value.
   */
  @Basic
  @Column(name = "GENDER")
  @Convert(converter = GenderAttribute::class)
  var gender = Gender.Male

  @Column(table = "SALARY")
  var salary = 0.0

  @Version
  var version: Long? = null

  @ManyToOne(cascade = [CascadeType.ALL])
  @JoinColumn(name = "MANAGER_ID")
  var manager: Employee? = null

  @OrderBy("lastName")
  @OneToMany(mappedBy = "manager")
  var managedEmployees: MutableSet<Employee> = mutableSetOf()

  @OneToMany(mappedBy = "owner", cascade = [CascadeType.ALL], orphanRemoval = true)
  @PrivateOwned
  var phoneNumbers: MutableList<PhoneNumber> = mutableListOf()

  @OneToOne(cascade = [CascadeType.ALL], fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinColumn(name = "ADDR_ID")
  var address: Address? = null

  @Embedded
  @AttributeOverrides(AttributeOverride(name = "startDate", column = Column(name = "START_DATE")), AttributeOverride(name = "endDate", column = Column(name = "END_DATE")))
  var period: EmploymentPeriod? = null

  @ElementCollection
  @CollectionTable(name = "RESPONS")
  var responsibilities: MutableList<String> = ArrayList()

  fun addManagedEmployee(employee: Employee): Employee {
    managedEmployees.add(employee)
    employee.manager = this
    return employee
  }

  fun removeManagedEmployee(employee: Employee): Employee {
    managedEmployees.remove(employee)
    employee.manager = null
    return employee
  }

  fun addPhoneNumber(phoneNumber: PhoneNumber): PhoneNumber {
    phoneNumbers.add(phoneNumber)
    phoneNumber.owner = this
    return phoneNumber
  }

  fun addPhoneNumber(type: String?, areaCode: String?, number: String?): PhoneNumber {
    val phoneNumber = PhoneNumber(type = type, areaCode = areaCode, number = number)
    return addPhoneNumber(phoneNumber)
  }

  fun removePhoneNumber(phoneNumber: PhoneNumber): PhoneNumber {
    phoneNumbers.remove(phoneNumber)
    phoneNumber.owner = null
    return phoneNumber
  }

  fun addResponsibility(responsibility: String) {
    responsibilities.add(responsibility)
  }

  fun removeResponsibility(responsibility: String) {
    responsibilities.remove(responsibility)
  }

  override fun toString(): String {
    return "Employee(id=$id, firstName=$firstName, lastName=$lastName, manager=${manager?.id}, managedEmployees=$managedEmployees)"
  }


}
