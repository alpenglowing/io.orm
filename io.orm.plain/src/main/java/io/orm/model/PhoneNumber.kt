package io.orm.model

import io.orm.model.type.UUIDAttribute
import io.orm.model.PhoneNumber.ID
import jakarta.persistence.*
import java.io.Serializable
import java.util.*

@Entity
@Table(name = "PHONE")
@IdClass(ID::class)
data class PhoneNumber(
  @Id
  @Column(name = "EMP_ID", updatable = false, insertable = false)
  @Convert(converter = UUIDAttribute::class)
  var id: UUID? = null,

  @Id
  @Column(updatable = false)
  var type: String? = null,

  @Basic
  @Column(name = "AREA_CODE")
  var areaCode: String? = null,

  @Basic
  @Column(name = "P_NUMBER")
  var number: String? = null
) {
  @ManyToOne
  @JoinColumn(name = "EMP_ID")
  var owner: Employee? = null
    set(value) {
      field = value
      value?.also { employee -> this.id = employee.id }
    }

  data class ID(
    var id: UUID = DefaultUUID,
    var type: String? = null
  ) : Serializable {
    override fun equals(other: Any?): Boolean {
      if (other is ID) {
        val otherID = other
        return otherID.id == id && otherID.type == type
      }
      return false
    }

    override fun hashCode(): Int {
      return super.hashCode()
    }
  }
}
