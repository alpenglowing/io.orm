package io.orm.model

import io.orm.model.type.UUIDAttribute
import jakarta.persistence.*
import jakarta.persistence.CascadeType.*
import org.eclipse.persistence.annotations.UuidGenerator
import java.util.*

@Entity(name = "author")
@Table(name = "authors")
@UuidGenerator(name = "author_uuid")
class Author(
  @Id
  @GeneratedValue(generator = "author_uuid")
  @Convert(converter = UUIDAttribute::class)
  var id: UUID? = null,

  var firstName: String? = null,
  var lastName: String? = null,

  @ManyToMany(mappedBy = "writers", targetEntity = Book::class, cascade = [PERSIST])
  var books: MutableSet<Book> = mutableSetOf()
)

fun Author.wrote(vararg _books: Book): Author {
  this.books.addAll(_books)
  _books.forEach { it.writers.add(this) }
  return this
}
