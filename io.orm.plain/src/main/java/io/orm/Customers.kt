package io.orm

import io.orm.jdbc.JdbcCustomers
import java.sql.Driver
import java.time.LocalDate
import java.util.*

interface Customer {
  var id: UUID
  var firstName: String
  var lastName: String
  var fiscalCode: String
  var birthDate: LocalDate
}

interface Customers {
  companion object {
    @JvmStatic
    fun jdbc(driver: Driver): Customers = JdbcCustomers(driver, "jdbc:h2:file://tmp/orm")
  }

  fun add(customer: Customer): Optional<UUID>
  fun edit(customer: Customer): Optional<Boolean>
  fun findBy(id: UUID): Optional<Customer>
  fun findAll(): Array<Customer>
}

