package io.orm.jdbc

import io.orm.Customer
import java.time.LocalDate
import java.util.*

data class CustomerDTO(
  override var id: UUID,
  override var firstName: String,
  override var lastName: String,
  override var fiscalCode: String,
  override var birthDate: LocalDate
) : Customer
