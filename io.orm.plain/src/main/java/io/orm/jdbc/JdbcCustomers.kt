package io.orm.jdbc

import io.orm.Customer
import io.orm.Customers
import java.sql.Date
import java.sql.Driver
import java.time.LocalTime.MIDNIGHT
import java.time.ZoneOffset.UTC
import java.util.*

class JdbcCustomers(private val driver: Driver, private val url: String) : Customers {
  override fun add(customer: Customer): Optional<UUID> {
    val connection = driver.connect(url, Properties())

    val statement = connection.prepareStatement("insert into customers(first_name, last_name, fiscal_code, birth_date) values (?, ?, ?, ?)")
    statement.setString(1, customer.firstName)
    statement.setString(2, customer.lastName)
    statement.setString(3, customer.fiscalCode)
    statement.setDate(4, Date(customer.birthDate.toEpochSecond(MIDNIGHT, UTC)))
    val executed = statement.executeUpdate()

    val uuid = Optional.of(executed)
      .filter { it > 0 }
      .map { statement.generatedKeys.getString(0) }
      .map { UUID.fromString(it) }

    statement.close()
    connection.close()

    return uuid
  }

  override fun edit(customer: Customer): Optional<Boolean> {
    val connection = driver.connect(url, Properties())

    val statement = connection.prepareStatement("update customers set first_name = ?, last_name = ?, fiscal_code = ?, birth_date = ? where id = ?")
    statement.setString(1, customer.firstName)
    statement.setString(2, customer.lastName)
    statement.setString(3, customer.fiscalCode)
    statement.setDate(4, Date(customer.birthDate.toEpochSecond(MIDNIGHT, UTC)))
    statement.setString(5, customer.id.toString())
    val executed = statement.executeUpdate()

    val uuid = Optional.of(executed)
      .filter { it > 0 }
      .map { statement.generatedKeys.getString(0) }
      .map { UUID.fromString(it) }

    statement.close()
    connection.close()

    return uuid
  }
}
