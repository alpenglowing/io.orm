package io.orm

import io.orm.model.*
import jakarta.persistence.Persistence

fun main() {
  val hermanMelville = Author(firstName = "Herman", lastName = "Melville")
  val johnWilliams = Author(firstName = "John", lastName = "Williams")

  val adelphi = Publisher(name = "adelphi")

  val mobyDick = Book(title = "Moby Dick")
  val stoner = Book(title = "Stoner")

  hermanMelville.wrote(mobyDick)
  johnWilliams.wrote(stoner)

  adelphi.published(stoner).published(mobyDick)

  val unit = Persistence.createEntityManagerFactory("library")
  val session = unit.createEntityManager()
  session.transaction.begin()
  session.persist(adelphi)
  session.transaction.commit()

  println("Adelphi: $adelphi")

  println("H. Melville: $hermanMelville")
  println("J. Williams: $johnWilliams")

  val foundStoner = session.find(Book::class.java, stoner.id)
  val foundMobyDick = session.find(Book::class.java, mobyDick.id)

  println("Libro cercato per id ${stoner.id} e ho trovato: $foundStoner")
  println("Libro cercato per id ${mobyDick.id} e ho trovato: $foundMobyDick")

  val einaudi = Publisher(name = "einaudi")
  einaudi.published(foundStoner)

  session.transaction.begin()
  session.persist(einaudi)
  session.transaction.commit()

  val foundEinaudi = session.find(Publisher::class.java, einaudi.id)

  println("Nuovo casa publicatrice: $foundEinaudi")
  println("Libro modificato: $foundStoner")
  foundEinaudi.books.forEach { println("Libro pubblicato: $it") }

  val jessicaPearson = Employee(firstName = "Jessica", lastName = "Pearson")
  val harveySpecter = Employee(firstName = "Harvey", lastName = "Specter")
  jessicaPearson.addManagedEmployee(harveySpecter)

  println("Default UUID: $DefaultUUID")

  session.transaction.begin()
  session.persist(harveySpecter)
  session.transaction.commit()

  println(session.find(Employee::class.java, harveySpecter.manager?.id))

  session.close()
  unit.close()
}
