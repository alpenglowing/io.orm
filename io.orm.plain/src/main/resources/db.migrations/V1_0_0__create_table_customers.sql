create table if not exists customers(
  id uuid not null default random_uuid(),
  first_name varchar(256) not null,
  last_name varchar(256) not null,
  fiscal_code varchar(17),
  birth_date timestamp,
  primary key (id),
  inserted_at timestamp not null default now(),
  updated_at timestamp not null,
  deleted_at timestamp not null
);

create table if not exists shops(
  id uuid not null default random_uuid(),
  code varchar(6) not null,

  primary key (id)
);

create table if not exists cards(
  barcode varchar(13) not null,
  enabled_at timestamp not null default now(),
  disabled_at timestamp not null,
  customer_id uuid not null,
  primary key (barcode),
  foreign key (customer_id) references customers(id)
);

create table if not exists locations(
  address varchar(256) not null,
  civic varchar(16) not null,
  zip_code varchar(8) not null,
  town varchar(128) not null,
  province varchar(2) not null,
  nation varchar(512) not null,
  customer_id uuid not null,
  unique (address, civic, zip_code, town, province, nation),
  foreign key (customer_id) references customers(id)
);


