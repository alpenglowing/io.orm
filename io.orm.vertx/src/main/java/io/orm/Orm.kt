package io.orm

import io.orm.model.Book
import io.vertx.core.AbstractVerticle
import io.vertx.core.Promise
import io.vertx.core.eventbus.EventBus
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import jakarta.persistence.EntityManager
import jakarta.persistence.EntityManagerFactory
import org.slf4j.LoggerFactory
import java.util.*

private val log = LoggerFactory.getLogger(Orm::class.java)

class Orm(private val unit: EntityManagerFactory) : AbstractVerticle() {
  private val session: EntityManager by lazy { unit.createEntityManager() }
  private val books: Books by lazy { Books.entities(vertx, session) }
  private val eventBus: EventBus by lazy { vertx.eventBus() }

  override fun start(start: Promise<Void>) {
    eventBus.localConsumer<JsonObject>("addBook") { addBook ->
      books.add(addBook.body().mapTo(Book::class.java))
        .onSuccess { addBook.reply(JsonObject.mapFrom(it)) }
        .onFailure { log.warn("Can't add book", it) }
    }

    eventBus.localConsumer<String>("findBookById") { findBookById ->
      books.findBy(UUID.fromString(findBookById.body()))
        .onSuccess { findBookById.reply(JsonObject.mapFrom(it)) }
        .onFailure { err -> System.err.println("Find-book-by-id failed ${err.message}").also { err.printStackTrace() } }
    }

    eventBus.localConsumer<String>("findBooks") { findBooks ->
      print("Looking for books... ")
      books.findAll()
        .onSuccess { books -> findBooks.reply(JsonArray(books.map { JsonObject.mapFrom(it) })) }
        .onFailure { err -> System.err.println("can't find books ${err.message}").also { err.printStackTrace() } }
        .onComplete { println("found books!") }
    }

    start.complete()
  }

  override fun stop(stop: Promise<Void>) {
    session.close()
    unit.close()
  }
}
