package io.orm

import io.orm.model.Book
import io.vertx.core.AbstractVerticle
import io.vertx.core.Promise
import io.vertx.core.eventbus.EventBus
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.shell.ShellService
import io.vertx.ext.shell.ShellServiceOptions
import io.vertx.ext.shell.command.CommandBuilder
import io.vertx.ext.shell.command.CommandRegistry
import io.vertx.ext.shell.term.TelnetTermOptions
import org.slf4j.LoggerFactory

private val log = LoggerFactory.getLogger(Shell::class.java)

class Shell : AbstractVerticle() {
  private val eventBus: EventBus by lazy { vertx.eventBus() }
  private val logo: String by lazy { vertx.fileSystem().readFileBlocking("logo").toString() }
  private val registry: CommandRegistry by lazy { CommandRegistry.getShared(vertx) }
  private val service: ShellService by lazy {
    ShellService.create(
      vertx, ShellServiceOptions()
        .setWelcomeMessage(logo)
        .setTelnetOptions(TelnetTermOptions()
          .setHost("localhost")
          .setPort(8085)
        )
    )
  }

  override fun start(start: Promise<Void>) {
    val addBook = CommandBuilder
      .command("add-book")
      .processHandler { process ->
        eventBus.request<JsonObject>("addBook", JsonObject.mapFrom(Book(title = process.args()[0])))
          .onSuccess { process.write("Add book: ${it.body()}\n") }
          .onFailure { process.write("Can't add book ${it.message}") }
          .onComplete { process.end(if (it.failed()) -1 else 0) }
      }
      .build(vertx)

    val findBooks = CommandBuilder
      .command("find-books")
      .processHandler { process ->
        eventBus.request<JsonArray>("findBooks", JsonObject())
          .onSuccess { process.write("Found books: ${it.body()}\n") }
          .onFailure { process.write("Can't find books ${it.message}") }
          .onComplete { process.end(if (it.failed()) -1 else 0) }
      }
      .build(vertx)

    registry
      .registerCommand(addBook)
      .onSuccess { log.info("Registered command add-book") }
      .onFailure { log.warn("Can't register command add-book", it) }

      .compose { registry.registerCommand(findBooks) }
      .onSuccess { log.info("Registered command find-books") }
      .onFailure { log.warn("Can't register command find-books", it) }

      .compose { service.start() }
      .onSuccess { log.info("Shell service started with telnet protocol on port 8085") }
      .onSuccess(start::complete)
      .onFailure(start::fail)
  }
}
