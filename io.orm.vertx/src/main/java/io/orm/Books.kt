package io.orm

import io.orm.model.Book
import io.vertx.core.Future
import io.vertx.core.Vertx
import jakarta.persistence.EntityManager
import org.slf4j.LoggerFactory
import java.util.*

private val log = LoggerFactory.getLogger(Books::class.java)

interface Books {
  companion object {
    @JvmStatic
    fun entities(vertx: Vertx, session: EntityManager): Books = Entities(vertx, session)
  }
  fun add(book: Book): Future<Book>
  fun findBy(id: UUID): Future<Book>
  fun findAll(): Future<List<Book>>
}

private class Entities(private val vertx: Vertx, private val session: EntityManager) : Books {
  override fun add(book: Book): Future<Book> = vertx.executeBlocking { update ->
    try {
      session.transaction.begin()
      session.persist(book)
      session.transaction.commit()
      update.complete(book)
    } catch (throwable: Throwable) {
      update.fail(OrmException(throwable))
    }
  }

  override fun findBy(id: UUID): Future<Book> = vertx.executeBlocking { query ->
    try {
      session
        .find(Book::class.java, id)
        ?.let { query.complete(it) }
        ?: query.fail("Can't find book with id $id")
    } catch (throwable: Throwable) {
      query.fail(OrmException(throwable))
    }
  }

  override fun findAll(): Future<List<Book>> = vertx.executeBlocking { query ->
    try {
      session
        .createQuery("select b from book b")
        .resultList
        .map { it as Book }
        .let { query.complete(it) }
    } catch (throwable: Throwable) {
      query.fail(OrmException(throwable))
    }
  }
}
