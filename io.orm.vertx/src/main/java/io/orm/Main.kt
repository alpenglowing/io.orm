package io.orm

import com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES
import com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.kotlinModule
import io.vertx.core.DeploymentOptions
import io.vertx.core.Vertx.vertx
import io.vertx.core.json.jackson.DatabindCodec
import jakarta.persistence.Persistence
import org.slf4j.LoggerFactory
import kotlin.reflect.KClass
import kotlin.system.exitProcess

private val log = LoggerFactory.getLogger("io.orm.main()")

fun main() {
  val vertx = vertx()
  val verticles = mutableMapOf<KClass<*>, String>()

  DatabindCodec.mapper()
    .registerModule(kotlinModule())
    .registerModule(JavaTimeModule())
    .disable(FAIL_ON_UNKNOWN_PROPERTIES)
    .disable(WRITE_DATES_AS_TIMESTAMPS)

  val unit = Persistence.createEntityManagerFactory("library")

  vertx
    .deployVerticle(Orm(unit), DeploymentOptions().setWorker(true))
    .onSuccess { verticles[Orm::class] = it }
    .compose { vertx.deployVerticle(Shell()) }
    .onSuccess { verticles[Shell::class] = it }
    .onFailure { verticles.values.forEach(vertx::undeploy) }
    .onComplete {
      when (it.succeeded()) {
        true -> log.info("ORM with Vert.x Integration started")
        else -> log.error("Can't start ORM with Vert.x Integration").also { vertx.close() }
      }
    }
}

class OrmException(cause: Throwable, message: String? = cause.message) : RuntimeException(message, cause)
